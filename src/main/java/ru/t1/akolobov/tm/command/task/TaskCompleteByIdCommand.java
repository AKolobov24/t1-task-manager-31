package ru.t1.akolobov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete task.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getAuthService().getUserId();
        getTaskService().changeStatusById(userId, id, Status.COMPLETED);
    }

}
