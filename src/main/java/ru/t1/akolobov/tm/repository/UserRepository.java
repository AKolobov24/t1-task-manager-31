package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        return models.stream()
                .filter(u -> login.equals(u.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        return models.stream()
                .filter(u -> email.equals(u.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return models.stream().anyMatch(u -> login.equals(u.getLogin()));
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return models.stream().anyMatch(u -> email.equals(u.getEmail()));
    }

}
