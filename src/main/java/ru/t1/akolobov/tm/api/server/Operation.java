package ru.t1.akolobov.tm.api.server;

import ru.t1.akolobov.tm.dto.request.AbstractRequest;
import ru.t1.akolobov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
