package ru.t1.akolobov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.ServerAboutRequest;
import ru.t1.akolobov.tm.dto.request.ServerVersionRequest;
import ru.t1.akolobov.tm.dto.response.ServerAboutResponse;
import ru.t1.akolobov.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request);

}
