package ru.t1.akolobov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;

public interface ICommand {

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    void execute();

    @Nullable
    Role[] getRoles();

}
