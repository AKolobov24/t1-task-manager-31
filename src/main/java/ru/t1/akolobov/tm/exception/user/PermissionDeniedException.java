package ru.t1.akolobov.tm.exception.user;

public final class PermissionDeniedException extends AbstractUserException {

    public PermissionDeniedException() {
        super("Error! Operation is not permitted...");
    }

}
