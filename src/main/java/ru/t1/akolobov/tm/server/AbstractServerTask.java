package ru.t1.akolobov.tm.server;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull Server server) {
        this.server = server;
    }

}
