package ru.t1.akolobov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.server.Operation;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.dto.request.AbstractRequest;
import ru.t1.akolobov.tm.dto.response.AbstractResponse;
import ru.t1.akolobov.tm.server.AbstractServerTask;
import ru.t1.akolobov.tm.server.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();
    @NotNull
    private final Bootstrap bootstrap;
    @Getter
    @Nullable
    private ServerSocket serverSocket;

    public Server(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        executorService.submit(task);
    }

    @NotNull
    public Object call(@NotNull AbstractRequest request) {
        return dispatcher.call(request);
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            Class<RQ> reqClass,
            Operation<RQ, RS> operation
    ) {
        dispatcher.registry(reqClass, operation);
    }

    @SneakyThrows
    public void start() {
        @NotNull IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull Integer port = propertyService.getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        executorService.shutdown();
        serverSocket.close();
    }

}
