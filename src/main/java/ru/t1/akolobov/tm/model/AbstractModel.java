package ru.t1.akolobov.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.model.IHasCreated;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractModel implements IHasCreated, Serializable {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Date created = new Date();

}
